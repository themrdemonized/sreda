<?php
class ControllerInformationSupport extends Controller {

	private $error = array();

	public function index() {
		$this->document->setTitle('Техподдержка');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$data['action'] = $this->url->link('information/support/sendToTrade', '', true);

		$this->response->setOutput($this->load->view('information/support', $data));
	}

	protected function validate() {
		if (utf8_strlen($this->request->post['name']) < 1) {
			$this->error['name'] = "Ваше имя введено неверно, пожалуйста, повторите ввод";
		}

		if (utf8_strlen($this->request->post['phone']) < 1) {
			$this->error['phone'] = "Ваш телефон введен неверно, пожалуйста, повторите ввод";
		}

		// Captcha
		// if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('contact', (array)$this->config->get('config_captcha_page'))) {
		// 	$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

		// 	if ($captcha) {
		// 		$this->error['captcha'] = $captcha;
		// 	}
		// }

		return !$this->error;
	}

	public function sendToTrade() {
		if (!(($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate())) {
			$json = array(
				'warning' => 'Обнаружены ошибки ввода'
			);
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode(array_merge($json, $this->error)));
			return;
		}

		$json = array(
			'phone' => $this->request->post['phone'],
			'name' => $this->request->post['name'],
			'success' => 'Ваша заявка была успешно отправлена, спасибо'
		);
		$mail = new Mail($this->config->get('config_mail_engine'));
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($this->config->get('config_email'));
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(html_entity_decode("Заявка на консультацию: " . $this->request->post['name'] . ", " . $this->request->post['phone'], ENT_QUOTES, 'UTF-8'));
		$mail->setText("Вам пришла новая заявка на консультацию:\nИмя: " . $this->request->post['name'] . ",\nТелефон: " . $this->request->post['phone']);
		$mail->send();

		$time = time();
    	$date = date('c', $time);
		$this->load->model('extension/module/trade_import');

    	$data = array();
    	$data['orders'] = array();
    	$data['orders'][] = array(
    		'contractor'	=> array(
    			'phone'		=> $this->request->post['phone'],
    			'email'		=> "",
    			'name'		=> $this->request->post['name'],
    			'address'	=> "",
    		),
    		'date' 			=> $date,
    		'description'	=> "Заявка на консультацию: " . $this->request->post['name'] . ", тел.: " . $this->request->post['phone']
    	);
    	$data['orders'][0]['goods'] = array();
    	
    	if (!file_exists("trade_import")) {
    		mkdir("trade_import", 0777, true);
    	}
    	$order_url = $this->config->get('module_trade_import_order_address');
    	$url = $this->config->get('module_trade_import_code');
    	$token = $this->config->get('module_trade_import_token');
    	$old_api_token = $this->config->get('module_trade_import_order_token');
    	if (!$this->config->get('module_trade_import_enable_old_api')) {
	    	$data_string = json_encode(array('token' => $token));
	    	$ch = curl_init();
			$header = array();
			$header[] = "Content-Type: application/json";
			$header[] = "UUID: " . $token;
			$header[] = "Timestamp: " . $date;
			$header[] = "Authorization: " . hash("sha512", $token . $time);
			$header[] = "Content-Length: " . strlen($data_string);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    	$response = curl_exec($ch);
	    	if ($response === false) {
				echo 'Curl error: ', curl_error($ch), "\n";
				file_put_contents("trade_import/error_" . $date . ".log", curl_error($ch) . "\n" . $response);
				$this->model_extension_module_trade_import->add_order($data_string, 0, 'ERROR: ' . $date . ".log");
	        	curl_close($ch);
	        	return 0;
	        } else {
	        	$response_decoded = json_decode($response, true);
                $access_token = $response_decoded['access_token'];
                curl_close($ch);
                $data_string = json_encode($data);
                $ch = curl_init();
                $header = array();
                $header[] = "Content-Type: application/json";
                $header[] = "Authorization: Bearer " . $access_token;
                curl_setopt($ch, CURLOPT_URL, $order_url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec($ch);
                curl_close($ch);
	        }
	    } else {
	    	$data_string = json_encode($data);
	    	$ch = curl_init();
			$header = array();
			$header[] = "Content-Type: application/json";
			$header[] = "UUID: " . $old_api_token;
			$header[] = "Timestamp: " . $date;
			$header[] = "Authorization: " . hash("sha512", $old_api_token . $time);
			$header[] = "Content-Length: " . strlen($data_string);
			curl_setopt($ch, CURLOPT_URL, $order_url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    	$response = curl_exec($ch);
	    	curl_close($ch);
	    }
        if (json_decode($response) !== NULL) {
        	$this->model_extension_module_trade_import->add_order($data_string, 0, $response);
        } else {
        	$this->model_extension_module_trade_import->add_order($data_string, 0, 'ERROR: ' . $date . ".log");
        	file_put_contents("trade_import/error_" . $date . ".log", $response);
        }

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
